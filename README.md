# Natural Computation Project

This repo contains the code and documentation of the project realized for the Natural Computation course, developed during my Master's Degree course in Computer Engineering.

# Racing Car Controller Optimization

To test the optimized controller obtained, the following steps must be followed:

1.  Open the terminal

2.  From the terminal go to the "carSim" folder

3.  From the "carSim" folder run the following command (make sure you are using Python 2.7):
    
    ```bat
    python custom_client.py -p <port> -f <filename_parameters>
    ```

    where:
    
        -   <port> should be replaced with the port number on which the selected server is running.

        -   <filename_parameter> must be replaced with the name of the file containing the optimized parameters you want to test.

            for this parameter it is possible to choose between the following values:

                -   best_etrack3: containing the optimized parameters on the E-Track3 track without opponents.
                -   best_forza: containing the optimized parameters on the Forza track without opponents.
                -   best_multi_obj: containing the optimized parameters on both tracks without opponents.
                -   best_multi_obj_opponents: containing the optimized parameters on both tracks with opponents.

    if both arguments ```<port>``` and ```<filename_parameters>``` are not set, the client will be launched on port ```3001```, with the controller parameters contained in the ```best_multi_obj_opponents``` file.
