import json
from custom_client import CustomClient


class CustomProblem:

    def __init__(self, perc=0.0, const=1, with_track_sensors=False, first_track_sensor=0.75, track_sensors_scale=1.7):
        self.perc = perc
        self.const = const
        self.params_names = []
        self.lower_bounds_params = []
        self.upper_bounds_params = []
        self.lower_bounds_track_sensors = []
        self.upper_bounds_track_sensors = []
        self.with_track_sensors = with_track_sensors

        with open('params_to_load', 'r') as p_read:
            params = json.load(p_read)

        for k, v in params.items():
            self.lower_bounds_params.append(v - abs(v * self.perc) - self.const)
            self.upper_bounds_params.append(v + abs(v * self.perc) + self.const)
            self.params_names.append(k)

        if self.with_track_sensors:
            while len(self.upper_bounds_track_sensors) != 9:
                if self.upper_bounds_track_sensors:
                    self.upper_bounds_track_sensors.append(self.upper_bounds_track_sensors[-1]*track_sensors_scale)
                else:
                    self.upper_bounds_track_sensors.append(first_track_sensor)

            self.lower_bounds_track_sensors = [0] + self.upper_bounds_track_sensors[:-1]

    def fitness(self, individual):

        P = dict()

        for i in range(len(individual[:len(self.params_names)])):
            P[self.params_names[i]] = individual[i]

        if self.with_track_sensors:
            track_sensors = list(individual[len(self.params_names):])
            reversed_track_sensors = track_sensors[:]
            reversed_track_sensors.reverse()

            track_sensors_list = [-item for item in reversed_track_sensors] + [0] + track_sensors
            track_sensors_string = ""

            for track_sensor in track_sensors_list:
                track_sensors_string += str(track_sensor) if track_sensor == track_sensors_list[-1] else str(track_sensor) + ' '

            c1 = CustomClient(port=3001, Params=P, threshold=300.0, cost1=100, cost2=1000, n_lap=2, track_sensors=track_sensors_string)
            c1.start()
            c1.join()

        else:
            c1 = CustomClient(port=3001, Params=P, threshold=300.0, cost1=100, cost2=1000, n_lap=2)
            c2 = CustomClient(port=3002, Params=P, threshold=300.0, cost1=100, cost2=1000, n_lap=2)
            c1.start()
            c2.start()
            c1.join()
            c2.join()

        with open('results.txt', 'a') as f_res:
            if self.with_track_sensors:
                f_res.write(str(P) + '\n' + track_sensors_string + '\n' + str(c1.fitness) + ' - ' + str(c2.fitness) + ' - ' + str(c1.fitness + c2.fitness) + '\n\n')
            else:
                f_res.write(str(P) + '\n' + str(c1.fitness) + ' - ' + str(c2.fitness) + ' - ' + str(c1.fitness + c2.fitness) + '\n\n')

        return [c1.fitness, c2.fitness]

    def get_bounds(self):

        return self.lower_bounds_params + self.lower_bounds_track_sensors, self.upper_bounds_params + self.upper_bounds_track_sensors

    def get_nobj(self):
        return 2
