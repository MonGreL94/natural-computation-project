import numpy as np
from matplotlib import pyplot as plt


def plot_pareto_frontier(obj1, obj2):

    sorted_list = sorted([[obj1[i], obj2[i]] for i in range(len(obj1))])
    pareto_frontier = [sorted_list[0]]
    for pair in sorted_list[1:]:
        if pair[1] <= pareto_frontier[-1][1]:
            pareto_frontier.append(pair)

    return pareto_frontier


def plot_statistics_single_evolution(times, upper_y, lower_y):

    generation_avg = np.array([np.mean(times[start:start+30]) for start in range(0, len(times), 30)])
    generation_std = np.array([np.std(times[start:start + 30]) for start in range(0, len(times), 30)])
    plt.plot([t for t in range(len(generation_avg))], generation_avg, color='b',
             label='mean fitness per gen.')
    plt.plot([t for t in range(len(generation_std))], generation_avg + generation_std, color='r',
             label='std fitness per gen.\n(mean std=' + str(round(float(np.mean(generation_std)), 4)) + ')')
    plt.plot([t for t in range(len(generation_std))], generation_avg - generation_std, color='r')
    plt.ylim([lower_y, upper_y])
    plt.legend()
    plt.grid()
    plt.xlabel('generations')
    plt.ylabel('mean fitness (sec)')
    plt.show()


def from_lines_to_times(lines, index_time=-1, threshold=300.0):

    times = []
    times_lines = lines[1::3]

    for times_line in times_lines:
        times_str = times_line.split(' - ')
        time = float(times_str[index_time])

        if time > threshold:
            time = threshold

        times.append(time)

    return times


def plot_statistics_multi_trial(trials_list):

    average_times = np.zeros(len(trials_list[0]))

    for trial in trials_list:
        average_times += trial

    average_times = average_times / float(len(trials_list))

    var_times = np.zeros(len(trials_list[0]))

    for t in range(len(average_times)):

        t_list = []

        for trial in trials_list:
            t_list.append(trial[t])

        var_times[t] = np.std(t_list)

    plt.plot([t for t in range(len(average_times))], average_times, color='b')
    plt.plot([t for t in range(len(average_times))], average_times - var_times, color='r')
    plt.plot([t for t in range(len(average_times))], average_times + var_times, color='r')
    plt.grid()
    plt.show()


def from_times_to_best_trend(times):

    times_post_processed = [max(times)]

    for time in times:
        if time < times_post_processed[-1]:
            times_post_processed.append(time)
        else:
            times_post_processed.append(times_post_processed[-1])

    return times_post_processed
