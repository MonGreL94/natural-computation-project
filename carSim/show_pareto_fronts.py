from matplotlib import pyplot as plt
import numpy as np
import math
from plot_utils import plot_pareto_frontier

with open('results.txt', 'r') as f:
    lines = f.read().split('\n')

n_lines = 3
p_size = 30
n_gen = 100

print 'individui valutati: ', (len(lines)) / n_lines
print 'generazioni valutate: ', (len(lines)) / n_lines / p_size - 1
print 'trial valutati: ', (len(lines) - 1) / n_lines / p_size / n_gen

times = []

x = []
y = []

a = lines[1::3]

for line in a:
    values = line.split(' - ')
    times.append(float(values[len(values)-1]))

    if float(values[0]) <= 300.0:
        x.append(float(values[0]))
    else:
        x.append(300.0)

    if float(values[1]) <= 300.0:
        y.append(float(values[1]))
    else:
        y.append(300.0)

times_avg = list(np.array(times)/2)

generations = []

for start in range(0, len(a), p_size):
    generations.append([x[start:start + p_size], y[start:start + p_size]])

front_to_plot = list()

best_generation = generations[int(math.ceil((times.index(min(times)) + 1)/30.0)) - 1]
best_generation_index = generations.index(generations[int(math.ceil((times.index(min(times)) + 1)/30.0)) - 1])
generations.remove(generations[int(math.ceil((times.index(min(times)) + 1)/30.0)) - 1])

last_generation = generations[-1]
generations.remove(generations[-1])

best_added = False

for j in range(int(len(generations)/3), len(generations), 13):

    if j >= best_generation_index and not best_added:
        front_to_plot.append(best_generation)
        best_added = True

    front_to_plot.append(generations[j])

if not best_added:
    if best_generation_index == n_gen:
        front_to_plot.append(last_generation)
        front_to_plot.append(best_generation)
    else:
        front_to_plot.append(best_generation)
        front_to_plot.append(last_generation)
else:
    front_to_plot.append(last_generation)

print min(times)
print min(times_avg)

min_x = 300
max_x = 0
min_y = 300
max_y = 0

color_scale = 1.0/float(len(front_to_plot))

color_red = 1
color_green = color_scale

print len(front_to_plot)

for obj1, obj2 in front_to_plot:

    pareto_front = plot_pareto_frontier(obj1, obj2)

    pf_X = [cops[0] for cops in pareto_front]
    pf_Y = [cops[1] for cops in pareto_front]

    if min(pf_X) < min_x:
        min_x = min(pf_X)

    if max(pf_X) > max_x:
        max_x = max(pf_X)

    if min(pf_Y) < min_y:
        min_y = min(pf_Y)

    if max(pf_Y) > max_y:
        max_y = max(pf_Y)

    plt.scatter(pf_X, pf_Y)
    plt.plot(pf_X, pf_Y, color=(abs(color_red), abs(color_green), 0))

    color_red -= color_scale
    color_green += color_scale

plt.xlabel('Objective 1: forza')
plt.ylabel('Objective 2: e-track-3')
plt.xlim([min_x-1, max_x+1])
plt.ylim([min_y-1, max_y+1])
plt.grid()
plt.show()
