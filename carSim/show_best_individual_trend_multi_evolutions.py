from matplotlib import pyplot as plt

lines_list = []

with open('results.txt', 'r') as f:
    lines_list.append(f.read().split('\n'))

n_lines = 3
p_size = 30
n_gen = 150

times_list = []
a_list = []

for lines in lines_list:
    a_list.append(lines[1::3])

for a in a_list:
    times = []
    for line in a:
        values = line.split(' - ')
        value = float(values[-1])
        times.append(value)

    times_list.append(times)

cnt = 0
min_times = 300.0

for times in times_list:
    cnt += 1

    local_min = min(times)

    if local_min < min_times:
        min_times = local_min

    chosen_time = times
    times_post_processed = [max(chosen_time)]

    for time in chosen_time:
        if time < times_post_processed[-1]:
            times_post_processed.append(time)
        else:
            times_post_processed.append(times_post_processed[-1])

    plt.plot([i for i in range(len(times_post_processed))], [element for element in times_post_processed], label='saDE - min=' + str(local_min))

plt.xlim([0, p_size * n_gen + p_size])
plt.ylim([min_times-2, 261])
plt.legend()
plt.grid()
plt.xlabel('valutations')
plt.ylabel('fitness (sec)')
plt.show()
