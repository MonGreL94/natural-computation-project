from plot_utils import plot_statistics_single_evolution

lines_list = []

with open('results.txt', 'r') as f:
    lines_list.append(f.read().split('\n'))

for lines in lines_list:
    n_lines = 3
    p_size = 30
    n_gen = 100

    print 'individui valutati: ', (len(lines)) / n_lines
    print 'generazioni valutate: ', (len(lines)) / n_lines / p_size - 1
    print 'trial valutati: ', (len(lines) - 1) / n_lines / p_size / n_gen

    times = []
    a = lines[1::3]

    for line in a:
        values = line.split(' - ')
        value = float(values[-1])

        if value > 300.0:
            value = 300.0

        times.append(value)

    print min(times)

    plot_statistics_single_evolution(times, 320, 170)
