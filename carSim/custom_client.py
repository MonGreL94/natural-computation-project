from threading import Thread
from snakeoil import Client
import math
from custom_client_utils import Track, initialize_car, drive
from server import Server
import sys
import json


class CustomClient(Client, Thread):

    def __init__(self, port=None, Params=None, threshold=None, cost1=1, cost2=100, n_lap=2, track_sensors=None, host=None, sid=None, ep=None, trackname=None, stage=None, debug=None, filename_params=None):
        self.threshold = threshold
        self.cost1 = cost1
        self.cost2 = cost2
        self.n_lap = n_lap
        self.fitness = None

        if track_sensors:
            self.sangs = [float(sensor) for sensor in track_sensors.split(' ')]
        else:
            self.sangs = [-45, -19, -12, -7, -4, -2.5, -1.7, -1, -.5, 0, .5, 1, 1.7, 2.5, 4, 7, 12, 19, 45]

        self.sangsrad = [(math.pi * X / 180.0) for X in self.sangs]

        self.T = Track()

        self.target_speed = 0
        self.lap = 0
        self.prev_distance_from_start = 1
        self.learn_final = False
        self.opHistory = list()
        self.trackHistory = [0]
        self.TRACKHISTORYMAX = 50
        self.secType = 0
        self.secBegin = 0
        self.secMagnitude = 0
        self.secWidth = 0
        self.badness = 0

        Client.__init__(self, host, port, sid, ep, trackname, stage, debug, filename_params, Params, track_sensors)
        Thread.__init__(self)

    def run(self):

        too_slow = False
        old_cur_lap_time = None
        cnt_lap = 0
        lap_times = []

        initialize_car(self)
        self.S.d['stucktimer'] = 0
        self.S.d['targetSpeed'] = 0

        for step in xrange(self.maxSteps, 0, -1):

            self.get_servers_input()

            if self.S.d['curLapTime'] < old_cur_lap_time:
                lap_times.append(self.S.d['lastLapTime'])
                cnt_lap += 1
                if cnt_lap == self.n_lap:
                    break

            old_cur_lap_time = self.S.d['curLapTime']

            if self.threshold and self.S.d['curLapTime'] + sum(lap_times) >= self.threshold:
                if self.S.d['lastLapTime'] == 0.0:
                    self.cost1 *= self.cost2
                too_slow = True
                break

            drive(self, step)
            self.respond_to_server()

        self.R.d['meta'] = 1
        self.respond_to_server()
        self.shutdown()

        result = self.cost1 * self.threshold if too_slow else sum(lap_times)

        print 'Risultato: ', result
        self.fitness = result


if __name__ == '__main__':

    try:
        port = int(sys.argv[2])
    except IndexError:
        port = 3001

    try:
        filename_params = sys.argv[4]
    except IndexError:
        filename_params = 'best_multi_obj_opponents'

    Server().start()
    c1 = CustomClient(port=port, filename_params=filename_params)
    c1.start()
    c1.join()
