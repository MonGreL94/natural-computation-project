import pygmo as pg
import numpy as np
from matplotlib import pyplot as plt
from problem import CustomProblem
from server import Server
import shutil
import os
import time
import json

n_trials = 1
p_size = 28
n_gens = 150

with open('results.txt', 'w') as f_res:
    f_res.write('')

pg.set_global_rng_seed(seed=32)

# The user-defined problem

# The pygmo problem
problem = CustomProblem(0.2, 1)
prob = pg.problem(problem)

# Base individual params
with open('best_pso_forza', 'r') as p_read_forza:
    base_params_forza = json.load(p_read_forza)

with open('best_pso_etrack3', 'r') as p_read_etrack3:
    base_params_etrack3 = json.load(p_read_etrack3)

base_individual_forza = []
base_individual_etrack3 = []

for k in problem.params_names:
    base_individual_forza.append(base_params_forza[k])
    base_individual_etrack3.append(base_params_etrack3[k])

# uda = pg.de(gen=n_gens, variant=2, F=0.9, CR=0.3, seed=1234)
uda = pg.moead(gen=n_gens, seed=1234)

algo = pg.algorithm(uda)
results_trial = []
evolutions = []

shutil.copy(os.path.join('C:/Program Files (x86)/torcs/config/raceman/0/quickrace.xml'),
            os.path.join('C:/Program Files (x86)/torcs/config/raceman/'))

Server().start()

time.sleep(1)

shutil.copy(os.path.join('C:/Program Files (x86)/torcs/config/raceman/1/quickrace.xml'),
            os.path.join('C:/Program Files (x86)/torcs/config/raceman/'))

Server().start()

for i in range(n_trials):
    print 'Trial number ' + str(i)
    # regulates both screen and log verbosity
    algo.set_verbosity(1)
    pop = pg.population(prob, p_size)
    pop.push_back(base_individual_forza)
    pop.push_back(base_individual_etrack3)
    algo.evolve(pop)
    log_trial = algo.extract(type(uda)).get_log()
    results_trial.append(log_trial[-1][2])
    evolutions.append(log_trial)

for evolution in evolutions:
    plt.plot(evolution[:, 1], evolution[:, 2], label=algo.get_name())
    plt.legend()
    plt.grid()
    plt.show()

print np.min(results_trial), np.max(results_trial), np.mean(results_trial), np.var(results_trial)
